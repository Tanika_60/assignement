package cs21120.assignment2018.impl;

import cs21120.assignment2018.IExpression;

import java.util.Iterator;

public class IExpressionImpl implements IExpression {


    @Override
    public int evaluateCountdown() throws Exception {
        return 0;
    }

    @Override
    public void set(IExpression left, IExpression right, Operation op) {

    }

    @Override
    public void set(int value) {

    }

    @Override
    public int getValue() throws Exception {
        return 0;
    }

    @Override
    public boolean isLeaf() {
        return false;
    }

    @Override
    public Iterator<IExpression> getIterator() {
        return null;
    }

    @Override
    public IExpression getLeft() {
        return null;
    }

    @Override
    public IExpression getRight() {
        return null;
    }

    @Override
    public Operation getOperation() {
        return null;
    }
}
