package cs21120.assignment2018;

import java.util.Iterator;

public class ExpressionWew8 implements IExpressionFactory {
    @Override
    public IExpression createRandomEquation(int[] vals) {
        return null;
    }

    @Override
    public IExpression findBestSolution(int[] vals, int target) {
        return null;
    }

    @Override
    public IExpression createLeaf(int val) {
        ExpressionTreeWew8 expressionTreeWew8 = new ExpressionTreeWew8();
        expressionTreeWew8.set(val);

        return expressionTreeWew8;
    }

    @Override
    public IExpression createInternalNode(IExpression l, IExpression r, IExpression.Operation op) {
        ExpressionTreeWew8 expressionTreeWew8 = new ExpressionTreeWew8();
        expressionTreeWew8.set(l, r, op);
        return expressionTreeWew8;
    }

    public static class ExpressionTreeWew8 implements IExpression {

        private int value;
        private Operation operation;
        private IExpression left;
        private IExpression right;

        private ExpressionTreeWew8() {
        }

        @Override
        public int evaluateCountdown() throws Exception {
            if(isLeaf()){return getValue();}
            int left = getLeft().evaluateCountdown();
            int right = getRight().evaluateCountdown();

            switch (this.operation){
                case MULTIPLY:
                    return left * right;
                case DIVIDE:
                    if(left % right == 0){ return left / right;}
                    throw new Exception(getLeft() + "/" + getRight() + "produce fraction");
                case SUBTRACT:
                    int i = left - right;
                    if(i >= 0){ return i;}
                    throw new Exception(getLeft() + "-" + getRight() + "produce negative value");
                case ADD:
                    return left + right;

            }
            return 0;
        }

        @Override
        public void set(IExpression left, IExpression right, Operation op) {
            this.left = left;
            this.right = right;
            this.operation = op;

        }

        @Override
        public void set(int value) {
            this.value = value;

        }

        @Override
        public int getValue() throws Exception {
            if (!isLeaf()) {
                return value;
            }

            throw new Exception(this + "It's not e leaf!");
        }

        @Override
        public boolean isLeaf() {
            return left == null && right == null;

        }

        @Override
        public Iterator<IExpression> getIterator() {
            return new ExpressionIterator(this);
        }

        @Override
        public IExpression getLeft() {
            return left;
        }

        @Override
        public IExpression getRight() {
            return right;
        }

        @Override
        public Operation getOperation() {
            return operation;
        }

        @Override
        public String toString() {
            final StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append('(').append(left.toString());
            switch (operation) {
                case MULTIPLY:
                    stringBuilder.append('*');
                    break;
                case DIVIDE:
                    stringBuilder.append('/');
                    break;
                case SUBTRACT:
                    stringBuilder.append('-');
                    break;
                case ADD:
                    stringBuilder.append('+');
                    break;
            }
            stringBuilder.append(right.toString()).append(')');

            return stringBuilder.toString();
        }
    }

}
